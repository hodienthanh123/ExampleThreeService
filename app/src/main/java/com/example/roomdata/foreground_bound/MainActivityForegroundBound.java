package com.example.roomdata.foreground_bound;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.roomdata.R;
import com.example.roomdata.foreground.entity.Song;
import com.example.roomdata.foreground_bound.servive.MyServiceGB;

public class MainActivityForegroundBound extends AppCompatActivity implements View.OnClickListener {
    private Button btnStasrtService, btnSopForeground, btnSopBoundSevice, btnStopServiceGB;
    private Song song;
    private MyServiceGB myServiceGB;
    private RelativeLayout layoutBottom;
    private TextView tvnameSong;
    private ImageView imgPlayOrPause;
    private boolean isServiceConnect;
    private ServiceConnection mServiceConnection =new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyServiceGB.MyBinder myBinder= (MyServiceGB.MyBinder) service;
            myServiceGB=myBinder.getMySV();
            isServiceConnect=true;

            handerLayouttMusic();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            myServiceGB=null;
            isServiceConnect=false;
        }
    };

    private void handerLayouttMusic() {
        layoutBottom.setVisibility(View.VISIBLE);
        tvnameSong.setText(myServiceGB.getmSong().getTitle());

        setImgViewPlayOrPause();
    }
    private void setImgViewPlayOrPause(){
        if(myServiceGB==null)
            return;
        imgPlayOrPause.setImageResource(myServiceGB.getisPlayer()? R.drawable.ic_pause : R.drawable.ic_play);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_forrground_background);
        init();
//        btnStasrtService.setOnClickListener(this);
//        btnSopForeground.setOnClickListener(Onclick1(btnSopBoundSevice.getId()));
        btnStasrtService.setOnClickListener(Onclick2(btnStasrtService));
        btnSopForeground.setOnClickListener(Onclick2(btnSopForeground));
        btnSopBoundSevice.setOnClickListener(Onclick2(btnSopBoundSevice));
        btnStopServiceGB.setOnClickListener(Onclick2(btnStopServiceGB));
        imgPlayOrPause.setOnClickListener(Onclick2(imgPlayOrPause));
    }
    private void init(){
        btnStasrtService=findViewById(R.id.btn_start_serviceGB);
        btnSopBoundSevice=findViewById(R.id.btn_stop_BoundService);
        btnSopForeground=findViewById(R.id.btn_stop_ForegroundService);
        btnStopServiceGB=findViewById(R.id.btn_stop_ServiceGB);
        tvnameSong=findViewById(R.id.tv_nameSong);
        layoutBottom=findViewById(R.id.layout_bottomGB);
        imgPlayOrPause=findViewById(R.id.img_play_or_paseGB);
    }
    public View.OnClickListener Onclick1(int id) {
        switch (id){
            case R.id.btn_start_serviceGB:{
                return v1 -> {
                    Log.d("onclick 1start service main---> ","start");
                };
            }
            case R.id.btn_stop_ForegroundService:{
                return v1 -> {
                    Log.d("onclick 1stop Foreground service main---> ","stop");
                };
            }
            case R.id.btn_stop_BoundService:{
                return v1 -> {
                    Log.d("onclick 1stop Bround service main---> ","stop");
                };
            }
        }
        return null;
    }

    public View.OnClickListener Onclick2 (View v){
        switch (v.getId()){
            case R.id.btn_start_serviceGB:{
                return v1 -> {
                    Log.d("onclick 2 start service main---> ","start");
                    onClickStartService();
                };
            }
            case R.id.btn_stop_ForegroundService:{
                return v1 -> {
                    Log.d(" onclick 2 stop Foreground service main---> ","stop");
                    onStopForegroundService();
                };
            }
            case R.id.btn_stop_BoundService:{
                return v1 -> {
                    Log.d("onclick 2 stop Bround service main---> ","stop");
                    onStopBoundService();
                };
            }
            case R.id.btn_stop_ServiceGB:{
                return v1 -> {
                    Log.d("onclick 2 stop service main---> ","stop");
                    onStopServiceGB();
                    layoutBottom.setVisibility(View.GONE);
                };
            }
            case R.id.img_play_or_paseGB:{
                return v1 -> {
                    Log.d("onclick 2 pause music main---> ","pause");
                    if(myServiceGB.getisPlayer())
                        myServiceGB.pauseMusic();
                    else
                        myServiceGB.resumeMusic();
                    setImgViewPlayOrPause();
                };
            }
        }
        return null;
    }

    private void onStopServiceGB() {
        //stop service foreground
        Intent intent =new Intent(this, MyServiceGB.class);
        stopService(intent);

        //stop bound service
        if(isServiceConnect){
            unbindService(mServiceConnection);
            isServiceConnect=false;
        }
    }

    private void onClickStartService() {
        Intent intent =new Intent(this, MyServiceGB.class);
        song=new Song("Thành ngháo", R.raw.aiaai);

        Bundle bundle=new Bundle();
        bundle.putSerializable("objSong",song);
        intent.putExtras(bundle);

        startService(intent);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void onStopForegroundService() {
        Intent intent =new Intent(this, MyServiceGB.class);
        stopService(intent);
        //isServiceConnect=false;
    }

    private void onStopBoundService() {
        if(isServiceConnect){
            unbindService(mServiceConnection);
            isServiceConnect=false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_start_serviceGB:{
                Log.d("onclick 0 start service main---> ","start");
                break;
            }
            case R.id.btn_stop_ForegroundService:{
                Log.d("onclick 0 stop foregroundservice main---> ","stop");
                break;
            }
            case R.id.btn_stop_BoundService:{
                Log.d("onclick  stop Bround service main---> ","stop");
                break;
            }
        }
    }
}