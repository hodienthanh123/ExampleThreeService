package com.example.roomdata.foreground_bound.servive;

import static com.example.roomdata.foreground_bound.MyApplication.CHANNEL_ID;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.roomdata.R;
import com.example.roomdata.foreground.entity.Song;

public class MyServiceGB extends Service {

    private MyBinder mBinder =new MyBinder();
    private Song mSong;
    private boolean isPlayer;
    private MediaPlayer mediaPlayer;
    public class MyBinder extends Binder{
        public MyServiceGB getMySV(){
            return MyServiceGB.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("myserviceGB--->>>" ," Ibind");
        return mBinder;
    }

    @Override
    public void onCreate() {
        Log.d("myserviceGB--->>>" ," create");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("myserviceGB--->>>" ," startcomand");
        Bundle  bundle =intent.getExtras();
        if(bundle !=null){
            mSong= (Song) bundle.get("objSong");
            startMusic(mSong);
            sendNotification(mSong);
        }
        return START_NOT_STICKY;
    }

    private void sendNotification(Song s) {
        NotificationCompat.Builder build=new NotificationCompat.Builder(this, CHANNEL_ID);
        build.setContentTitle(getString(R.string.app_name));
        build.setContentText(s.getTitle());
        build.setSmallIcon(R.drawable.ic_small_music);
        Notification h=build.build();
        startForeground(1, h);
    }
    private void startMusic(Song mSong){
        if(mediaPlayer==null){
            mediaPlayer=   MediaPlayer.create(getApplicationContext(),mSong.getResourc());
        }
        mediaPlayer.start();
        isPlayer=true;
    }
    public void pauseMusic(){
        if(mediaPlayer!=null && isPlayer){
            mediaPlayer.pause();
            isPlayer=false;
        }
    }
    public void resumeMusic(){
        if(mediaPlayer!=null && !isPlayer){
            mediaPlayer.start();
            isPlayer=true;
        }
    }
    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("myserviceGB--->>>" ," onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d("myserviceGB--->>>" ," destroy service");
        if(mediaPlayer!=null){
            mediaPlayer.release();
            mediaPlayer=null;
        }
        super.onDestroy();
    }

    public void setmSong(Song mSong) {
        this.mSong = mSong;
    }

    public Song getmSong() {
        return mSong;
    }

    public boolean getisPlayer() {
        return isPlayer;
    }
}
