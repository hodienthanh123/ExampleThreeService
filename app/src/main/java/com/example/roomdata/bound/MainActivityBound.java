package com.example.roomdata.bound;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Button;

import com.example.roomdata.R;
import com.example.roomdata.bound.servicebound.MusicBoundService;

public class MainActivityBound extends AppCompatActivity {
    private Button btnStart, btnStop;
    private MusicBoundService musicBoundService;
    private boolean isServiceConnect;
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // giao tiếp với boundService
            MusicBoundService.MyBinder myBinder = (MusicBoundService.MyBinder) service;
            //Nếu đã có binder thì get service để thao tác
            musicBoundService = myBinder.getMusicBoundService();
            musicBoundService.startMusic();
            isServiceConnect = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isServiceConnect = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_bound);

    btnStart=findViewById(R.id.btn_start_services);
    btnStop=findViewById(R.id.btn_stop_services);

        btnStart.setOnClickListener(v->{
        onClickStartService();
    });
        btnStop.setOnClickListener(v->{
        onClickStopService();
    });
}
    private void onClickStartService() {
        Intent intent = new Intent(this, MusicBoundService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }
    private void onClickStopService() {
        if(isServiceConnect){
            unbindService(mServiceConnection);
            isServiceConnect=false;
        }
    }
}