package com.example.roomdata.bound.servicebound;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class ServiceConnectM {
    private Messenger mMessage;
    private boolean isServiceConnect;
    public ServiceConnectM(  boolean isServiceConnect) {
        this.isServiceConnect = isServiceConnect;
    }

    private ServiceConnection m =new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            //mMessage=new Messenger(service);
            setmMessage(new Messenger(service));
            Log.d("ngắt kết nối ----> ",""+getisServiceConnect());
            setServiceConnect(true);
            sendServiceMessage();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            setmMessage(null);
            //mMessage=null;
            setServiceConnect(false);
        }
    };
    private void sendServiceMessage() {
        Message message= Message.obtain(null, MusicServiceM.MSG_PLAYMUSIC,0,0);
        try {
            getmMessage().send(message);
        //mMessage.send(message);
        }
        catch (RemoteException e){
            e.printStackTrace();
        }
    }

    public Messenger getmMessage() {
        return mMessage;
    }

    public void setmMessage(Messenger mMessage) {
        this.mMessage = mMessage;
    }

    public boolean getisServiceConnect() {
        return isServiceConnect;
    }

    public void setServiceConnect(boolean serviceConnect) {
        isServiceConnect = serviceConnect;
    }

    public ServiceConnection getM() {
        return m;
    }
}

