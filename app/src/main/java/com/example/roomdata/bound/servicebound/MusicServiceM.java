package com.example.roomdata.bound.servicebound;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.roomdata.R;

public class MusicServiceM extends Service {
    public static final int MSG_PLAYMUSIC=1;
    public static final int MSG_STOPMUSIC=2;
    private MediaPlayer mMediaPlayer;

    private Messenger messenger;

    public class MyHandeler extends Handler{
        private Context applicationContext;

        public MyHandeler(Context context) {
            this.applicationContext = context.getApplicationContext();
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
             switch (msg.what){
                 case MSG_PLAYMUSIC:{
                    startMusic();
                    break;
                 }
                 default:
                     super.handleMessage(msg);
             }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        messenger=new Messenger(new MyHandeler(this));
        Log.d("MusicServiceM-->" ,"onBind");
        return messenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MusicServiceM-->" ,"onCreate");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("MusicServiceM-->" ,"onUnBind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d("MusicServiceM-->" ,"onDestroy");
        super.onDestroy();
        if(mMediaPlayer!=null){
            mMediaPlayer.release();
            mMediaPlayer=null;
        }
    }

    private void startMusic(){
        if(mMediaPlayer== null){
            mMediaPlayer=MediaPlayer.create(getApplicationContext(), R.raw.ngvm);
        }
        mMediaPlayer.start();
        Log.d("MusicServiceM -->> ","start nmusic");
    }
}
