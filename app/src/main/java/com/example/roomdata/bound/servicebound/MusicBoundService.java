package com.example.roomdata.bound.servicebound;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.roomdata.R;

public class MusicBoundService extends Service {
    private MyBinder myBinder =new MyBinder();
    private MediaPlayer mMediaPlayer;

    public class MyBinder extends Binder {
        public  MusicBoundService getMusicBoundService(){
            return MusicBoundService.this;
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("MusicBoundService start -->> ","OnBind");
        return myBinder;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MusicBoundService start -->> ","Oncreate");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("MusicBoundService start -->> ","onUnBind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d("MusicBoundService start -->> ","destroy Bind");
        super.onDestroy();
        if(mMediaPlayer!=null) {
            mMediaPlayer.release();
            mMediaPlayer=null;
        }
    }
    public void startMusic(){
        if(mMediaPlayer== null){
            mMediaPlayer=MediaPlayer.create(getApplicationContext(), R.raw.ngvm);
        }
        mMediaPlayer.start();
        Log.d("MusicService -->> ","start nmusic");
    }
}
