package com.example.roomdata.bound;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Button;

import com.example.roomdata.R;
import com.example.roomdata.bound.servicebound.MusicServiceM;
import com.example.roomdata.bound.servicebound.ServiceConnectM;

public class MainActivityBoundMessage extends AppCompatActivity {
    private Button btnStartMes, btnStopM;
    private Messenger mMessage;
    private ServiceConnectM classserviceConnectM;
    private boolean isServiceConnect;

//    private ServiceConnection mServiceConnection=new ServiceConnection() {
//        @Override
//        public void onServiceConnected(ComponentName name, IBinder service) {
//            mMessage=new Messenger(service);
//            isServiceConnect=true;
//            sendServiceMessage();
//        }
//
//        @Override
//        public void onServiceDisconnected(ComponentName name) {
//            mMessage=null;
//            isServiceConnect=false;
//        }
//    };
//
    private void sendServiceMessage() {
        Message message= Message.obtain(null, MusicServiceM.MSG_PLAYMUSIC,0,0);
        try {
            mMessage.send(message);
        }
        catch ( RemoteException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_bound_message);
        btnStartMes=findViewById(R.id.btn_start_servicem);
        btnStopM=findViewById(R.id.btn_stop_servicem);
        classserviceConnectM=new ServiceConnectM(isServiceConnect);
        btnStartMes.setOnClickListener(v->{
            startServiceM();
        });
        btnStopM.setOnClickListener(v->{
            stopServiceM();
        });
    }

    private void startServiceM() {
        Log.d("kết nối main status isConnect ----> ",""+isServiceConnect);
        Intent intent=new Intent(this, MusicServiceM.class);
        if(!isServiceConnect)
            isServiceConnect=true;
        bindService(intent, classserviceConnectM.getM() , Context.BIND_AUTO_CREATE);
    }

    private void stopServiceM() {
        if(classserviceConnectM.getisServiceConnect()){
            Log.d("ngắt kết nối main ----> ",""+isServiceConnect);
            unbindService(classserviceConnectM.getM());
            isServiceConnect=false;
            classserviceConnectM.setServiceConnect(false);
        }
    }

}