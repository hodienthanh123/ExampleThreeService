package com.example.roomdata.foreground.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int action_musics= intent.getIntExtra("action_music", 0);

        Intent intService =new Intent(context, MyService.class);
        intService.putExtra("action_music_service", action_musics);

        Log.d("MyReceiver-----> ",""+action_musics);
        //start nhiều lần, sẽ không khởi tạo nhiều lần, chỉ khởi tạo duy nhất mootj lần
        context.startService(intService);
    }
}
