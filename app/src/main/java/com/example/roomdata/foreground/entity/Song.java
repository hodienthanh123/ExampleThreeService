package com.example.roomdata.foreground.entity;

import java.io.Serializable;

public class Song implements Serializable {
    private String title;
    private String single;
    private int img;
    private int resourc;

    public Song(String title, String single, int img, int resourc) {
        this.title = title;
        this.single = single;
        this.img = img;
        this.resourc = resourc;
    }

    public Song(String title, int resourc) {
        this.title = title;
        this.resourc = resourc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSingle() {
        return single;
    }

    public void setSingle(String single) {
        this.single = single;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getResourc() {
        return resourc;
    }

    public void setResourc(int resourc) {
        this.resourc = resourc;
    }
}
