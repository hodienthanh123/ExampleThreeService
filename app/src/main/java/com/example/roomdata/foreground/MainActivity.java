package com.example.roomdata.foreground;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.roomdata.R;
import com.example.roomdata.foreground.entity.Song;
import com.example.roomdata.foreground.service.MyService;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {

    private EditText txt;
    private Button btnCre, btnDes;
    private Button btnStartService, btnStopService;
    /////////////////////////////////////////
    private RelativeLayout layoutBottom;
    private ImageView  imgSong, imgPlayOrPause, imgClear;
    private TextView tvTitleSong, tvSingleSong;
    ///////////////
    private Song s;
    private boolean isPlaying;
    //action thì dùng tham số không khai báo, nhận dữ liệu từ service sang
    private BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //hàm nhận được dữ liệu
            Bundle bundle=intent.getExtras();
            if(bundle==null)
                return;
            s= (Song) bundle.get("obj_Song");
            isPlaying=bundle.getBoolean("status_Player");
            int actionMusic=bundle.getInt("action_Music");
            // xử lý hiện thị layout ở dưới
            handleLayoutMusic(actionMusic);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt=findViewById(R.id.edt_data_intent);
        btnStartService=findViewById(R.id.btn_start_service);
        btnStopService=findViewById(R.id.btn_stop_service);
        imgSong=findViewById(R.id.img_song);
        imgPlayOrPause=findViewById(R.id.img_play_or_pause);
        imgClear=findViewById(R.id.img_clear);
        tvTitleSong=findViewById(R.id.tv_title_song);
        tvSingleSong=findViewById(R.id.tv_single_song);
        layoutBottom=findViewById(R.id.layout_bottom);

        btnStartService.setOnClickListener(v->{
            ClickstartService();
        });
        btnStopService.setOnClickListener(v->{
            ClickstopService();
        });
        // lắng nghe LocalBroadcast bằng registerReceiver kèm intent(action) nào?
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("send_data_to_activity"));
    }

    private void ClickstartService() {
        Song s=new Song("Anh thề đấy","đức huy",R.drawable.imgnhac,  R.raw.aiaai);
        Intent intent=new Intent(this, MyService.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("objSong", s);
        intent.putExtras(bundle);
        //intent.putExtra("key_data_intent",txt.getText().toString().trim());
        startService(intent);
    }
    private void ClickstopService() {
        Intent intent=new Intent(this, MyService.class);
        stopService(intent);
    }
    private void handleLayoutMusic(int ac){
        switch (ac){
            //sữa đổi trạng thái -> MyService.action_Pause.....
            //trạng thái start
            case 4: //MyService.action_Start.....
                {
                layoutBottom.setVisibility(View.VISIBLE);
                showInfoSong();
                setStatusBttunPhayOrPause();
                break;
            }
            //status action_Pause
            case 1:{
                setStatusBttunPhayOrPause();
                break;
            }
            //status action_Resume
            case 2:{
                setStatusBttunPhayOrPause();
                break;
            }
            //status action_Clear
            case 3:{
                layoutBottom.setVisibility(View.GONE);
                break;
            }

        }
    }
    private void showInfoSong(){
        if(s==null){
            return;
        }
        imgSong.setImageResource(s.getImg());
        tvTitleSong.setText(s.getTitle());
        tvSingleSong.setText(s.getSingle());

        imgPlayOrPause.setOnClickListener(v->{
            if(isPlaying){
                //status: pasue=1, resume=2, clear=3, start=4
                senActionToService(1);
            }
            else {
                senActionToService(2);
            }
        });
        imgClear.setOnClickListener(v->{
            senActionToService(3);
        });
    }
    private void setStatusBttunPhayOrPause(){
//        if(isPlaying){
//            imgPlayOrPause.setImageResource(R.drawable.lpay);
//        }
//        else{
//            imgPlayOrPause.setImageResource(R.drawable.pause);
//        }
        imgPlayOrPause.setImageResource(isPlaying ? R.drawable.lpay:R.drawable.pause);
    }
    private void senActionToService(int ac){
        Intent intent=new Intent(this, MyService.class);
        intent.putExtra("action_music_service", ac);
        startService(intent);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //khi destroy thì cần un registerReceiver LocalBroadcas
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }
}