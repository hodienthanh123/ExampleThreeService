package com.example.roomdata.foreground.service;

import static com.example.roomdata.foreground.MyApplication.Channel_ID;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.roomdata.R;
import com.example.roomdata.foreground.entity.Song;

public class MyService extends Service {
    private static final int action_Pause=1;
    private static final int action_Resume=2;
    private static final int action_Clear=3;
    private static final int action_Start=4;
    private MediaPlayer mediaPlayer;
    private boolean isPlaying;
    private Song msong;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Trạng thái: create ---->>>>","thành công myServivce");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //return super.onStartCommand(intent, flags, startId);
        //String dtaIn=intent.getStringExtra("key_data_intent");
        Bundle b=intent.getExtras();
        if(b!=null){
            Song s= (Song) b.get("objSong");
            if(s!=null){
                msong=s;
                //Log.d("title Song----> ", ""+s.getTitle());
                //Log.d("single Song----> ", ""+s.getSingle());
                //Log.d("resource Song----> ", ""+s.getResourc());
                startMusic(s);
                sendNotification(s);

            }
        }
        //sendNotification(dtaIn);

        int actionMusic=intent.getIntExtra("action_music_service", 0);
        Log.d("Chuyển trạng thái music --->>>> ","đã chuyển: "+actionMusic);
        handleMusic(actionMusic);
        return START_NOT_STICKY;
    }

    private void startMusic(Song s) {
        if(mediaPlayer==null)
            mediaPlayer=MediaPlayer.create(getApplicationContext(), s.getResourc());
        mediaPlayer.start();
        isPlaying=true;
        sendacitonToActovity(action_Start);
        Log.d("resource Song----> ", "Đã start music "+isPlaying);
    }
    private void handleMusic(int action){
        switch (action){
            case action_Pause: {
                pauseMusic();
                break;
            }
            case action_Resume:
                resumeMusic();
                break;

            case action_Clear:
                clearMusic();
                break;

        }
    }

    private void pauseMusic(){
        if(mediaPlayer!=null && isPlaying){
            mediaPlayer.pause();
            isPlaying=false;
            sendNotification(msong);
            sendacitonToActovity(action_Pause);
            Log.d("vô trong pause--->>> ",""+isPlaying);
        }
        else
            Log.d("không có dữ liệu status--->>> ","trong pause "+isPlaying);
    }
    private void resumeMusic(){
        if(mediaPlayer!=null && !isPlaying){
            mediaPlayer.start();
            isPlaying=true;
            sendNotification(msong);
            sendacitonToActovity(action_Resume);
            Log.d("vô trong resume--->>> ",""+isPlaying);
        }
        else
            Log.d("k có dữ liệu status--->>> ","ressume "+isPlaying);
    }
    private void clearMusic(){
        stopSelf();
        sendacitonToActovity(action_Clear);
        Log.d("status--->>> ","tắt service");
    }
    //custom graphic music
//    private void sendNotification(Song s) {
//
//        Intent intent=new Intent(this, MainActivity .class);
//
//        PendingIntent pendingIntent ;
//        pendingIntent =  PendingIntent.
//                getActivity(this, 0,intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        Bitmap bitmap= BitmapFactory.decodeResource(getResources(), s.getImg());
//
    //layout custom
//        RemoteViews rmView = new RemoteViews(getPackageName(), R.layout.custom_notification);
//        rmView.setTextViewText(R.id.tv_title_song, s.getTitle());
//        rmView.setTextViewText(R.id.tv_single_song, s.getSingle());
//        rmView.setImageViewBitmap(R.id.img_song, bitmap);
//
//        rmView.setImageViewResource(R.id.img_play_or_pause, R.drawable.lpay);
//
//        if(isPlaying){
//            Log.d("status send--->>> ","Nhạc chạy: "+isPlaying);
    //          set action in img or more
//            rmView.setOnClickPendingIntent(R.id.img_play_or_pause, getPendingIntent(this,action_Pause));
//
//            rmView.setImageViewResource(R.id.img_play_or_pause, R.drawable.lpay);
//        }
//        else{
//            rmView.setOnClickPendingIntent(R.id.img_play_or_pause, getPendingIntent(this,action_Resume));
//
//            rmView.setImageViewResource(R.id.img_play_or_pause, R.drawable.pause);
//            Log.d("status send--->>> ",""+"nhạc dừng   "+isPlaying);
//        }
////        rmView.setOnClickPendingIntent(R.id.img_play_or_pause, getPendingIntent(this,isPlaying ?action_Pause: action_Resume));
////        rmView.setImageViewResource(R.id.img_play_or_pause,isPlaying? R.drawable.pause: R.drawable.lpay);
//
//        rmView.setOnClickPendingIntent(R.id.img_clear, getPendingIntent(this,action_Clear));
//
//        Notification notification=new NotificationCompat.Builder(this, Channel_ID)
////                .setContentTitle("title notification example")
////                .setContentText(dtaIn)
//                .setSmallIcon(R.drawable.ic_notification)
//                .setContentIntent(pendingIntent)  /// quay trở lại activity
//                .setCustomContentView(rmView)     //Đỗ ra giao diện custom
////                .setSound(null)
//                .build();
//
//        startForeground(1, notification);
//        //stopSelf();
//    }
    private void sendNotification(Song s) {

        MediaSessionCompat mediaSessionCompat=new MediaSessionCompat(this,"tag");
        Bitmap bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.imgnhac);
        // set mặc định không có sử lý logic
//        Notification notification=new NotificationCompat.Builder(this, Channel_ID)
//                .setSmallIcon(R.drawable.ic_small_music)
//                .setSubText("Thành ngáo")
//                .setContentTitle(s.getTitle())
//                .setContentText(s.getSingle())
//                .setLargeIcon(bitmap)
//                ///action media controll
//                .addAction(R.drawable.ic_previous,"Previous", null) //#0
////                .addAction(R.drawable.ic_pause, "Pause", null)
//                .addAction(R.drawable.ic_pause, "Pause", getPendingIntent(this, action_Pause))//#1
//                .addAction(R.drawable.ic_next, "Next", null)    //#2
//                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
//                            .setShowActionsInCompactView(0,1,2 /*#1 pasuw button*/)//có thể hiện thị nhiều và ngăn cách nhau bằng dấu phẩy
//                            .setMediaSession(mediaSessionCompat.getSessionToken()))
//                .build();
//        NotificationManagerCompat managerCompat=NotificationManagerCompat.from(this);
//        managerCompat.notify(1, notification);

        //tách ra để dùng lồng hàm sử lý logic
        NotificationCompat.Builder notificationBuilder=new NotificationCompat.Builder(this, Channel_ID)
                .setSmallIcon(R.drawable.ic_small_music)
                .setSubText("Thành ngáo")
                .setContentTitle(s.getTitle())
                .setContentText(s.getSingle())
                .setLargeIcon(bitmap)
                ///action media controll

                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(0,1,2 ,3/*#1 pasuw button*/)//có thể hiện thị nhiều và ngăn cách nhau bằng dấu phẩy
                        .setMediaSession(mediaSessionCompat.getSessionToken()));

        if(isPlaying){
            notificationBuilder
                    .addAction(R.drawable.ic_previous,"Previous", null) //#0
                    .addAction(R.drawable.ic_pause, "Pause", getPendingIntent(this, action_Pause))//#1
                    .addAction(R.drawable.ic_next, "Next", null)    //#2
                    .addAction(R.drawable.close, "Next", getPendingIntent(this, action_Clear)) ;//#3
        }
        else{
            notificationBuilder
                    .addAction(R.drawable.ic_previous,"Previous", null) //#0
                    .addAction(R.drawable.ic_play, "Pause", getPendingIntent(this, action_Resume))//#1
                    .addAction(R.drawable.ic_next, "Next", null) ;   //#2
                    //.addAction(R.drawable.close, "Next", getPendingIntent(this, action_Clear)) ;
        }
        Notification notification=notificationBuilder.build();
        startForeground(1, notification); // thông báo hiện thị notification
    }

    private PendingIntent getPendingIntent(Context context, int action) {
        Log.d("status pending--->>>getPendingInten----> ",""+isPlaying);
        Intent intent = new Intent(this, MyReceiver.class);
        intent.putExtra("action_music", action);
        return PendingIntent.getBroadcast(context.getApplicationContext(), action, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
    // send status actiom from activity
    private void sendacitonToActovity(int action){
        Intent intent=new Intent("send_data_to_activity");
        Bundle bundle=new Bundle();
        bundle.putSerializable("obj_Song", msong);
        bundle.putBoolean("status_Player",isPlaying);
        bundle.putInt("action_Music", action);
        intent.putExtras(bundle);

        //sử dụng broadcasst đến activity, kèm status layoutbottom trong activity
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);


    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Destroy: -------> ","thành công myServivce");
        if(mediaPlayer!=null) {
            mediaPlayer.release();;
            mediaPlayer=null;
        }
    }
}
