package com.example.roomdata.backgroundservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.roomdata.R;
import com.example.roomdata.backgroundservice.service.MyServiceBgr;
import com.example.roomdata.foreground_bound.servive.MyServiceGB;

public class MainActivityBackgroundService extends AppCompatActivity {
    private Button btnStartService, btnStopSV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_background_service);
        init();
        btnStartService.setOnClickListener(OnclickBtn(btnStartService));
        btnStopSV.setOnClickListener(OnclickBtn(btnStopSV));
    }
    private void init(){
        btnStartService=findViewById(R.id.start_serviceBgr);
        btnStopSV=findViewById(R.id.stop_serviceBgr);
    }
    private View.OnClickListener OnclickBtn(View v){
        switch (v.getId()){
            case R.id.start_serviceBgr:{
                return v1->{
                    Intent intent=new Intent(this, MyServiceBgr.class);
                    startService(intent);
                };
            }
            case R.id.stop_serviceBgr:{
                return v1->{
                    Intent intent =new Intent(this, MyServiceBgr.class);
                    stopService(intent);
                    //stopService()
                };
            }
        }
        return null;
    }
}