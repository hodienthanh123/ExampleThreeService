package com.example.roomdata.backgroundservice.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.roomdata.R;

public class MyServiceBgr extends Service {

    private MediaPlayer mediaPlayer;
    private boolean isPlayer;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("BgrmyserviceGB--->>>" ," startcomand");
        startMusic();
        return START_NOT_STICKY;
    }

    private void startMusic(){
        if(mediaPlayer==null){
            mediaPlayer= MediaPlayer.create(getApplicationContext(), R.raw.aiaai);
        }
        mediaPlayer.start();
        isPlayer=true;
    }

    @Override
    public void onCreate() {
        Log.d("Trạng thái: create ---->>>>","backgroundSV");
        super.onCreate();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("BgrMyservice--->>>" ," onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d("Destroy: -------> ","backgroundSV");
        super.onDestroy();
        if(mediaPlayer!=null){
            mediaPlayer.release();
            mediaPlayer=null;
        }
    }
}
