package com.example.roomdata.bacground_solutionjobsheduler.service;

import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.roomdata.R;

public class MuServiceBSJ extends JobService {
    private static final String TAG="MuServiceBSJ----> ";
    private boolean jobCancelled;
    private MediaPlayer mMediaPlayer;
    private boolean isPlayer;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG,"startJob");
        Log.d("----> ","startJob");
        doBackGroundWork(params);
        return true;
    }

    private void doBackGroundWork(JobParameters params) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for(int i=0;i<19;i++){
//                    if(jobCancelled){
//                        return;
//                    }
//                    Log.d(TAG,"run: "+i);
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//                Log.d(TAG,"run: "+"job finished");
//                jobFinished(params, false);
//            }
//        }).start();
        new Thread(() -> {
            startMusic();
            Log.d(TAG,"run: "+"job finished trong thread"+mMediaPlayer.getCurrentPosition());
             for(int i=0;i<mMediaPlayer.getDuration();i++){
                if(jobCancelled){
                    return;
                }
                Log.d(TAG,"run: "+i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Log.d(TAG,"run: "+"JobParameters--> "+params.getJobId());
            Log.d(TAG,"run: "+"job finished trong thread"+mMediaPlayer.getDuration());
            jobFinished(params, false); //có lập lịch lập lại không true/false
            //Log.d(TAG,"run: "+"JobParameters--> "+params.getJobId());
        }).start();
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG,"jobStopped onStop");
        jobCancelled=true;
        if(mMediaPlayer!=null){
            mMediaPlayer.release();
            mMediaPlayer=null;
            isPlayer=false;
            Log.d(TAG,"stop music");
        }
        return true;
    }
    private void startMusic(){
        if(mMediaPlayer==null){
            mMediaPlayer=MediaPlayer.create(getApplicationContext(), R.raw.aiaai);
        }
        mMediaPlayer.start();
        isPlayer=true;
    }
}
