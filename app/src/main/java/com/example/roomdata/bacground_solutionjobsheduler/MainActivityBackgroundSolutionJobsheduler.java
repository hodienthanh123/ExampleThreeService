package com.example.roomdata.bacground_solutionjobsheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.roomdata.R;
import com.example.roomdata.bacground_solutionjobsheduler.service.MuServiceBSJ;
import com.example.roomdata.backgroundservice.service.MyServiceBgr;

public class MainActivityBackgroundSolutionJobsheduler extends AppCompatActivity {
    private static final int JOB_ID = 123123;
    private Button btnStartSV, btnStopSV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_background_solution_jobsheduler);
        init();
        btnStartSV.setOnClickListener(OnclickBtn(btnStartSV));
        btnStopSV.setOnClickListener(OnclickBtn(btnStopSV));

    }
    private void init(){
        btnStartSV=findViewById(R.id.start_serviceBSJ);
        btnStopSV=findViewById(R.id.stop_serviceBSJ);
    }
    private View.OnClickListener OnclickBtn(View v){
        switch (v.getId()){
            case R.id.start_serviceBSJ:{
                return v1->{
                    startGSJService();
                };
            }
            case R.id.stop_serviceBSJ:{
                return v1->{
                    stopBSJService();
                };
            }
        }
        return null;
    }

    private void startGSJService() {
        Log.d("----> ","startJob");
        ComponentName nameComponent= new ComponentName(this, MuServiceBSJ.class);
        JobInfo jobInfo=new JobInfo.Builder(JOB_ID, nameComponent)
                .setRequiresCharging(false) // hoạt động khi chỉ cắm sặc
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setPersisted(true) ///hoạt đồng khi devier sập nguồn và khởi động lại và hoạt động bình thường
                .setPeriodic(15 * 60 * 1000) //thời gian hoạt động 1 lần theo khoảng thười gian(định kỳ)
                .build();
        JobScheduler jobScheduler= (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(jobInfo);
        Log.d("----> ","startJob");
    }

    private void stopBSJService() {
        JobScheduler jobScheduler= (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(JOB_ID);
    }
}